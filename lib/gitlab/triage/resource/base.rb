require_relative '../url_builders/url_builder'

module Gitlab
  module Triage
    module Resource
      class Base
        attr_reader :resource, :net

        def initialize(new_resource, new_net)
          @resource = new_resource
          @net = new_net
        end

        private

        def network
          net[:network]
        end

        def url(params = {})
          UrlBuilders::UrlBuilder.new(
            net_opts.merge(params: { per_page: 100 }.merge(params))
          ).build
        end

        def net_opts
          {
            host_url: net[:host_url],
            api_version: net[:api_version],
            resource_type: self.class.name.demodulize.underscore.pluralize,
            source: source,
            source_id: resource[:"#{source.singularize}_id"]
          }
        end

        def source
          if resource[:project_id]
            'projects'
          elsif resource[:group_id]
            'groups'
          end
        end
      end
    end
  end
end
