require_relative 'base'
require_relative 'milestone'
require_relative 'instance_version'

module Gitlab
  module Triage
    module Resource
      class Context < Base
        EvaluationError = Class.new(RuntimeError)

        def eval(ruby)
          instance_eval <<~RUBY
            begin
              #{ruby}
            rescue StandardError, ScriptError => e
              raise EvaluationError.new(e.message)
            end
          RUBY
        rescue EvaluationError => e
          # This way we could obtain the original backtrace and error
          # If we just let instance_eval raise an error, the backtrace
          # won't contain the actual line where it's giving an error.
          raise e.cause
        end

        private

        def instance_version
          @instance_version ||= InstanceVersion.new(net)
        end

        def milestone
          @milestone ||=
            resource[:milestone] && Milestone.new(resource[:milestone], net)
        end
      end
    end
  end
end
