# frozen_string_literal: true

require_relative 'base'
require_relative '../command_builders/text_content_builder'
require_relative '../command_builders/comment_command_builder'
require_relative '../command_builders/label_command_builder'
require_relative '../command_builders/remove_label_command_builder'
require_relative '../command_builders/cc_command_builder'
require_relative '../command_builders/status_command_builder'

module Gitlab
  module Triage
    module Action
      class Comment < Base
        class Dry < Comment
          def act
            puts "\nThe following comments would be posted for the rule **#{name}**:\n\n"

            super
          end

          private

          def perform(resource, comment)
            puts "# #{resource[:web_url]}\n```\n#{comment}\n```\n"
          end
        end

        def act
          resources.each do |resource|
            comment = build_comment(resource).strip

            perform(resource, comment) unless comment.empty?
          end
        end

        private

        def build_comment(resource)
          CommandBuilders::CommentCommandBuilder.new(
            [
              CommandBuilders::TextContentBuilder.new(rule[:comment], resource: resource, net: net).build_command,
              CommandBuilders::LabelCommandBuilder.new(rule[:labels]).build_command,
              CommandBuilders::RemoveLabelCommandBuilder.new(rule[:remove_labels]).build_command,
              CommandBuilders::CcCommandBuilder.new(rule[:mention]).build_command,
              CommandBuilders::StatusCommandBuilder.new(rule[:status]).build_command
            ]
          ).build_command
        end

        def perform(resource, comment)
          net[:network].post_api(
            net[:token],
            build_post_url(resource),
            body: comment)
        end

        def build_post_url(resource)
          # POST /projects/:id/issues/:issue_iid/notes
          post_url = UrlBuilders::UrlBuilder.new(
            host_url: net[:host_url],
            api_version: net[:api_version],
            source_id: net[:source_id],
            resource_type: type,
            resource_id: resource['iid'],
            sub_resource_type: 'notes'
          ).build

          puts Gitlab::Triage::UI.debug "post_url: #{post_url}" if net[:debug]

          post_url
        end
      end
    end
  end
end
