require 'spec_helper'

require 'gitlab/triage/filters/ruby_conditions_filter'
require 'gitlab/triage/network'
require 'gitlab/triage/network_adapters/httparty_adapter'

describe Gitlab::Triage::Filters::RubyConditionsFilter do
  include_context 'network'

  let(:resource) do
    { milestone: milestones.first }
  end

  let(:milestones) do
    [
      {
        id: 1,
        project_id: 2,
        start_date: '2018-01-01'
      },
      {
        id: 3,
        project_id: 2,
        start_date: '2018-02-01'
      }
    ]
  end

  let(:condition) do
    { ruby: 'Date.today > milestone.succ.start_date' }
  end

  subject { described_class.new(resource, condition, net) }

  it_behaves_like 'a filter'

  before do
    allow(network).to receive(:print)
  end

  context '#calculate' do
    it 'evaluates the expression properly' do
      stub_api(
        :get, 'http://test.com/api/v4/projects/2/milestones',
        query: { per_page: 100, state: 'active' }) { milestones }

      expect(subject.calculate).to be_truthy
    end

    context 'when there is an internal error' do
      before do
        expect_next_instance_of(Gitlab::Triage::Resource::Context) do |context|
          def context.milestone
            raise NameError
          end
        end
      end

      it 'captures the backtrace pointing to where the actual error raised' do
        expect { subject.calculate }.to raise_error(NameError) { |error|
          expect(error.backtrace.first).to include('milestone')
        }
      end
    end
  end
end
