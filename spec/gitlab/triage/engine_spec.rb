require 'spec_helper'

require 'gitlab/triage/engine'
require 'gitlab/triage/network_adapters/test_adapter'

describe Gitlab::Triage::Engine do
  Options = Struct.new(:dry_run, :project_id, :token, :debug, :host_url)
  let(:token) { 'token' }
  let(:policies) { {} }
  let(:options) { Options.new(false, 42, 'abc123', false, 'https://gitlab.com') }

  subject { described_class.new(policies: policies, options: options, network_adapter_class: Gitlab::Triage::NetworkAdapters::TestAdapter) }

  describe '#initialize' do
    describe 'mandatory params' do
      context 'with no project_id given' do
        let(:options) { Options.new(false, nil, 'abc123', false) }

        it { expect { subject }.to raise_error(ArgumentError, 'A project_id is needed (pass it with the `--project-id` option)!') }
      end

      context 'with no token given' do
        let(:options) { Options.new(false, 42, nil, false) }

        it { expect { subject }.to raise_error(ArgumentError, 'A token is needed (pass it with the `--token` option)!') }
      end
    end

    describe 'default values' do
      it 'sets default values for host_url, api_version, and per_page' do
        expect(subject.host_url).to eq('https://gitlab.com')
        expect(subject.api_version).to eq('v4')
        expect(subject.per_page).to eq(100)
      end

      context 'with host url param given' do
        let(:options) { Options.new(false, 42, 'abc123', false, 'https://labgit.com') }

        it { expect(subject.host_url).to eq('https://labgit.com') }
      end

      context 'with host url in policies file' do
        let(:policies) do
          {
            host_url: 'https://labgit.com'
          }
        end

        it { expect(subject.host_url).to eq('https://labgit.com') }
      end

      it 'sets options.dry_run in when TEST is true' do
        expect(subject.options.dry_run).to eq(true)
      end
    end
  end

  describe '#perform' do
    it 'prints what it does to stdout' do
      expected_message = <<~MESSAGE
        Performing a dry run.

        =========================
        Triaging the `42` project
        =========================

      MESSAGE

      expect { subject.perform }.to output(expected_message).to_stdout
    end

    context 'with rules' do
      let(:policies) do
        {
          resource_rules: {
            issues: {
              rules: [
                {
                  name: 'Rule 1',
                  actions: {
                    comment: 'Hello World!'
                  }
                }
              ]
            }
          }
        }
      end

      it 'prints what it does to stdout' do
        expected_message = <<~MESSAGE
          Performing a dry run.

          =========================
          Triaging the `42` project
          =========================

          ---------------------------
          Processing rules for issues
          ---------------------------

          ---------------------------
          Processing rule: **Rule 1**
          ---------------------------

          .

          * Found 0 resources...
          * Filtering resources...
          * Total after filtering: 0 resources
          * Limiting resources...
          * Total after limiting: 0 resources

          The following comments would be posted for the rule **Rule 1**:

        MESSAGE

        expect { subject.perform }.to output(expected_message).to_stdout
      end
    end

    describe 'milestone condition back-compatibility' do
      let(:policies) do
        {
          resource_rules: {
            issues: {
              rules: [
                {
                  name: 'Rule 1',
                  conditions: {
                    milestone: [
                      'v1'
                    ]
                  },
                  actions: {
                    comment: 'Hello World!'
                  }
                }
              ]
            }
          }
        }
      end

      it 'pass the correct "milestone" param to the API call' do
        expect(Gitlab::Triage::UrlBuilders::UrlBuilder).to receive(:new)
          .with(a_hash_including(params: { per_page: 100, 'milestone' => 'v1' }))
          .and_call_original

        expect { subject.perform }.to output.to_stdout
      end
    end
  end
end
