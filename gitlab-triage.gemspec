lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/triage/version'

Gem::Specification.new do |spec|
  spec.name          = 'gitlab-triage'
  spec.version       = Gitlab::Triage::VERSION
  spec.authors       = ['GitLab']
  spec.email         = ['remy@rymai.me']

  spec.summary       = 'GitLab triage automation project.'
  spec.homepage      = 'https://gitlab.com/gitlab-org/gitlab-triage'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '~> 5.1'
  spec.add_dependency 'httparty', '~> 0.15'

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'gitlab-styles', '~> 2.1'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'webmock', '~> 3.4'
end
